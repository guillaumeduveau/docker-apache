FROM alpine:3.6

LABEL "com.example.vendor"="Guillaume Duveau" \
      description="Apache."

# Ensure www-data user exists
RUN addgroup -g 82 -S www-data && \
    adduser -u 82 -D -S -G www-data www-data

# Install packages
RUN apk upgrade --no-cache && \
    apk add --no-cache \
            bash \
            ca-certificates \
            openssl \
            apache2 \
            apache2-proxy \
            apache2-ssl

# Configure Apache
RUN mkdir -p /run/apache2 && \
    sed -i 's,#LoadModule slotmem_shm_module modules/mod_slotmem_shm.so,LoadModule slotmem_shm_module modules/mod_slotmem_shm.so,g' /etc/apache2/httpd.conf && \
    sed -i 's,LoadModule mpm_prefork_module modules/mod_mpm_prefork.so,#LoadModule mpm_prefork_module modules/mod_mpm_prefork.so,g' /etc/apache2/httpd.conf && \
    sed -i 's,#LoadModule mpm_event_module modules/mod_mpm_event.so,LoadModule mpm_event_module modules/mod_mpm_event.so,g' /etc/apache2/httpd.conf && \
    sed -i 's,#LoadModule expires_module modules/mod_expires.so,LoadModule expires_module modules/mod_expires.so,g' /etc/apache2/httpd.conf && \
    sed -i 's,#LoadModule rewrite_module modules/mod_rewrite.so,LoadModule rewrite_module modules/mod_rewrite.so,g' /etc/apache2/httpd.conf && \
    sed -i 's,User apache,User www-data,g' /etc/apache2/httpd.conf && \
    sed -i 's,Group apache,Group www-data,g' /etc/apache2/httpd.conf && \
    sed -i 's,#ServerName www.example.com:80,ServerName localhost:80,g' /etc/apache2/httpd.conf

# Redirect Apache logs
RUN sed -ri \
		     -e 's!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g' \
		     -e 's!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g' \
         "/etc/apache2/httpd.conf"

# Run Apache as UID=1000, GID=1000
RUN deluser www-data && \
    addgroup -g 1000 -S www-data && \
    adduser -u 1000 -D -S -s /bin/bash -G www-data www-data && \
    sed -i '/^www-data/s/!/*/' /etc/shadow

# Docker entrypoint
COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

# Expose ports
EXPOSE 80 443
