![coverage](https://gitlab.com/guillaumeduveau/docker-apache/badges/master/build.svg)

This Apache Docker image :

+ is based on Alpine
+ runs Apache in mpm-event mode, for coupling with a PHP-FPM image
+ runs Apache with UID=1000 and GID=1000 (which is generally corresponding to the user/group of your own user in Ubuntu, so you don't have any permission problems)
+ has mod-rewrite enabled
+ is for dev purposes, NOT PRODUCTION
